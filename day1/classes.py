class Mammal:
    def __init__(self):
        self.Species = "unknown"
        self.Talk = "unknown"

    def speak(self):
        return self.Talk

    def species(self):
        return self.Species


class Human(Mammal):
    def __init__(self, name):
        self.name = name

    def species(self):
        return 'Human'

    def speak(self):
        return f'My name is {self.name}'

class Cat(Mammal):
    def __init__(self):
        self.Species = "Cat"
        self.Talk = "Meow"

class Dog(Mammal):
    def __init__(self):
        self.Species = "Dog"
        self.Talk = "Woof"



