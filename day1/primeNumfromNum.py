def prompter():
    "Function to display prompt, check if an integer has been entered and parses the value"
    try:
        A= input('Please enter a number: ')
        A = int(A)
    except ValueError:
        print("\n-----Invalid entry! Please enter a number!-----\n")
        prompter()
    else:
        return A
        
def modeSelector():
    "Mode selection for prime number(s) finder"
    my_mode = 0
    while(my_mode==0):
        try:
            print("\nWELCOME TO PRIME NUMBER FINDER!\n")
            print("Mode 1: will show all prime numbers up to your selected number\nMode 2: will show you the nearest prime number between 0 and your number.\n")
            my_mode=input("Please enter your preferred mode: ")
            temp=int(my_mode)
            if temp == 1 or temp == 2:
                my_mode = temp
                break
        except ValueError:
            pass
    return my_mode

def primeNumbers():
    "Prime Number Finder"
    mode = modeSelector()
    target = prompter()
    num_to_print = []
    for number in range(0, target + 1):
        if number > 1:
            for i in range(2, number):
                if (number % i) == 0:
                    break
            else:
                num_to_print.append(number)
    if mode == 1:
        print(num_to_print)
    elif mode ==2:
        print(num_to_print[-1])
    else:
        print("error in mode\n\n\n\nRebooting game...\n\n\n\n")
        primeNumbers()
    prompt = input("Do you want me to run again?: ")
    if prompt[0] == 'y' or prompt[0] == 'Y':
        primeNumbers()
    else:
        print("SAFE!")


if __name__=="__main__":
    primeNumbers()
