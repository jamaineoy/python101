import sys

print(f"This many parameters: {len(sys.argv)-1}")

for item in sys.argv:
    if item == sys.argv[0]:
        continue
    else:
        print(f"A word: {item}")