# while loops
import random
import sys
import myfunc as mf

plays = 0
while(True):
    if plays == 0:
        print("\nWelcome To Number Guesser!\n")
    else:
        print(f"\nRound {plays+1}!\n")

    plays+=1
    secret = random.randint(0,99)
    correct = 0
    count = 0
    while(correct == 0):
        guess = mf.prompter()
        if guess < 0:
            print("The secret is not negative!")
            continue
        count+=1
        if guess > secret:
            print(">>> Too High")
        elif guess < secret:
            print(">>> Too Low")
        else:
            print(">>> Well Done")
            print(f'You took {count} guesses')
            correct = 1
    response=input("Do you want to play again?:")
    if response == 'y':
        continue
    else:
        if plays == 1:
            games = "game"
        else:
            games = "games"
        print(f"\nYou played {plays} {games} of Number Guesser! GOODBYE!")
        sys.exit()