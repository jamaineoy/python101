def square(n):
    return n*n

def is_negative(n):
    if n < 0:
        return True
    else:
        return False

def is_even(n):
    if n % 2 == 0:
        return True
    else:
        return False

l = range(10)
#for item in l:
#    print(f"Value {item} squared is {item * item}")
squares=[]
#for item in l:
#    a=square(item)
#    print(f"{item} squared = {a}")
#    squares.append(a)
#print(f'Square number list: {squares}')

squares = list(map(square,l))
print(f'Square number list: {squares}')

even_squares = list(filter(is_even, squares))
print(f'Even Square numbers from list: {even_squares}')

squares = list(map(lambda x:x*x,l))
print(f'Square number list: {squares}')

even_squares = list(filter(lambda x:x%2==0, squares))
print(f'Even Square numbers from list: {even_squares}')