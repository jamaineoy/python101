import re
PATTERN = '(\d+)'
lines = list(map(lambda x: x.strip(), open('data.txt').readlines()))
for line in lines:
    match = re.search(PATTERN, line)
    if match:
        print(f"Found: {match.group(0)}")
    else:
        print(f"Did not match line: {line}")


# print(lines)