import collatzprompt as cp 

def collatz(number):
    "Collatz Conjecture Runner"
    print(number)
    if number > 1:
        if number % 2 == 0:
            number = int(number / 2)
        else:
            number = (number * 3) + 1
        
        collatz(number)

if __name__=="__main__":
    user_input = cp.prompter()
    collatz(user_input)