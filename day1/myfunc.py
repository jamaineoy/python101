def prompter():
    "Function to display prompt, check if an integer has been entered and parses the value"
    try:
        A= input('Please enter your guess:')
        A = int(A)


    except ValueError:
        print("\n-----Invalid entry! Please enter a number!-----\n")
        prompter()
    else:
        return A
