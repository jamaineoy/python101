# Read from file
file = open('data.txt')

# content = file.readlines()
content = file.readlines()
# print(content)

for line in content:
    line = line.strip()
    print(f"This line is {len(line)} characters long.")
    print(line)