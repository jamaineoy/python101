import json

file = open('json.txt')

content = file.read()
print(f'I read in some content. It is  of type {type(content)}')

parse = json.loads(content)
print(f'Parsed content is {parse}')
print(f'Parsed content type is {type(parse)}')

count = len(parse)
print(f'There are {count} items in the parsed list')

author = parse[0]['a']
print(f'Author of quote: {author}')