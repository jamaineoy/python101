import random
import string
import datetime

timestamp_file = "saved_time_stamp.txt"
password_file = "password.txt"

def randompasswordgenerator(length):
    # Get the current datetime when the function is called
    timeNow = datetime.datetime.now()

    # assigns a string to variables that only contain appropriate ascii or custom characters
    uletters = string.ascii_uppercase 
    lletters = string.ascii_lowercase
    digits = string.digits
    # special = string.punctuation
    special = '!"#$%&?@'  # custom declared strings for special characters


    # declares empty string: password
    password = ''
    for i in range(0,length):
        random_number = random.randint(1, 4)
        # appends either upper/lower case letters, digits, custom special characters to password
        if random_number == 1:
            password = password + f"{str(random.sample(lletters, 1)[0])}"
        elif random_number == 2:
            password = password + f"{str(random.sample(digits, 1)[0])}"
        elif random_number == 3:
            password = password + f"{str(random.sample(uletters, 1)[0])}"
        elif random_number == 4:
            password = password + f"{str(random.sample(special, 1)[0])}"

    open(timestamp_file, "w").write(str(timeNow)) # save password creation timestamp
    open(password_file, "w").write(password) # write password to file
    return password


if __name__ == '__main__':
    try:
        rotation = 5 # currently set to minutes, change to 2 days
        #saved_time_stamp = open(timestamp_file, "r").read()
        imported_time_timestamp = datetime.datetime.strptime(open(timestamp_file, "r").read(),"%Y-%m-%d %H:%M:%S.%f") # convert previous timestamp to datetime object for comparison
        allow_change_timestamp = imported_time_timestamp + datetime.timedelta(minutes=rotation) # Sets rotation allowance | currently set to minutes, change to 2 days
        current_timestamp = datetime.datetime.now() # gets current datetime

        # print(f'saved_time_stamp: {saved_time_stamp}')
        # print(f'allow_change_timestamp: {allow_change_timestamp}')
        # print(f'current_timestamp: {current_timestamp}')
        # print(f'difference: {current_timestamp-imported_time_timestamp}')
        # print(current_timestamp >= imported_time_timestamp)


        if current_timestamp >= allow_change_timestamp: # compares the current timestamp to the previous timestamp, if TRUE, password rotation is executed.
            randompasswordgenerator(10)
            print('RDS password changed')
        else:
            print('RDS password change not allowed')


    except FileNotFoundError:
        print('no RDS password found, generating new password')
        randompasswordgenerator(10)
        