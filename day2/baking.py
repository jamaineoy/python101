""" A primitive naan-baking library """
def make_dough(in1,in2):
    """ Make dough if given the correct ingredients. Return True on success, False otherwise """
    in1 = in1.lower().strip()
    in2 = in2.lower().strip()
    if (in1 == 'water' and in2 == 'flour') or (in1 == 'flour' and in2 == 'water'):
        return True
    
    return False


def make_pizza(in1):
    in1 = in1.lower().strip()
    if in1 == 'dough':
        return True
    
    return False

def make_new_pizza(in1,in2):
    make_new_dough = make_dough(in1,in2)
    if make_new_dough == True:
        make_pizza('dough')
    
    return False




if __name__=='__main__':
    pass