import json
import requests
import sys
import random
def country_info():
    parse = requests.get('https://restcountries.eu/rest/v2/all')
    parse = parse.json()
    country = random.randint(0,len(parse))
    # flag = parse[country]['flag']
    name = parse[country]['name']
    lang = parse[country]['languages']
    Ccode = parse[country]['alpha2Code'].lower()
    language = []
    for i in range(len(lang)):
        language.append(parse[country]['languages'][i]['name'])

    langstr = "Language(s) Spoken:"
    wiki_name = name.strip().replace(" ", "_")
    wiki_link = "https://en.wikipedia.org/wiki/" + wiki_name

    for i in language:
        langstr=langstr + " " + i

    Name=name
    Message=langstr + ". Here is a link for our country's wikipedia page: " + wiki_link
    Icon=Ccode
    Icon = "https://flagcdn.com/128x96/" + Icon + ".png"

    return Name, Message, Icon

SLACK_URL = open("slackurl").read() #PASTE SLACK URL HERE
Name, Message, Icon = country_info()
# message = "Some words here, perhaps including content from the API response"
payload = """
{
 "channel": "#academy_api_testing",
 "username": """
payload += '"' + Name + '"'
payload += ""","""
payload += """ "text": """
payload += '"' + Message + '"'
payload += """,
 "color": "warning",
 "icon_url": """
payload += '"' + Icon + '"' + """
}"""

# print(payload)
# prompt = input('Do you wish to POST [1 to post,anything to cancel]:')
# if prompt == '1':
print(Name)
r = requests.post(SLACK_URL, data=payload)
# else:
sys.exit()
########
# checkfor r.status_code == 200 ?