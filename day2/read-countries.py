import json
import requests
from string import ascii_lowercase
# RUN THE FOLLOWING COMMAND IN TERMINAL FIRST
# $ curl  https://restcountries.eu/rest/v2/all | jq . > countries.json
# file = open('countries.json')
# content = file.read()
# parse = json.loads(content)
parse = requests.get('https://restcountries.eu/rest/v2/all').json()
# parse = json.loads(requests.get('https://restcountries.eu/rest/v2/all').text)
print(parse[0])
list_of_countries = []
countries_starting_with_letter = []
regions = []


letter = input("enter a letter: ").lower()[0]
for country in range(len(parse)):
    list_of_countries.append(parse[country]["name"])

    regions.append(parse[country]["region"])

    if parse[country]["name"][0].lower() == letter:
         countries_starting_with_letter.append(parse[country]["name"])
txt = ['are','countries']
if len(countries_starting_with_letter) == 1:
    txt = ['is','country']
for item in countries_starting_with_letter:
    print(item)
print(f'There {txt[0]} {len(countries_starting_with_letter)} {txt[1]} starting with {letter.upper()}')

regions = set(regions)
# print(regions)

