import random
import string

def get_random_string(length):
    # choose from all lowercase letter
    uletters = string.ascii_uppercase
    lletters = string.ascii_lowercase
    digits = string.digits
    special = string.punctuation
    # if length % 4 == 0:
    #     division = int(length / 4)
    # elif length % 3 == 0:
    #     division = int(length / 3)
    # else:
    #     division = int(length / length)
    result_str = ''
    for i in range(0,length):
        random_number = random.randint(1, 3)
        if random_number == 1:
            result_str = result_str + f"{str(random.sample(lletters, 1)[0])}"
        elif random_number == 2:
            result_str = result_str + f"{str(random.sample(digits, 1)[0])}"
        elif random_number == 3:
            result_str = result_str + f"{str(random.sample(uletters, 1)[0])}"
        elif random_number == 4:
            result_str = result_str + f"{str(random.sample(special, 1)[0])}"
        # if i % 2 != 0 and i % 3 != 0: 
        #     result_str = result_str + f"{str(random.sample(letters, 1)[0])}"
        #     # result_str = ''.join(random.sample(letters, 1))
        # elif i % 2 == 0 and i % 3 != 0:
        #     result_str = result_str + f"{str(random.sample(digits, 1)[0])}"
        #     # result_str = ''.join(random.sample(digits, 1))
        # elif i % 2 != 0 and i % 3 == 0:
        #     result_str = result_str + f"{str(random.sample(special, 1)[0])}"
            # result_str = ''.join(random.sample(special, 1))
        # else:
        #     result_str = result_str + f"{str(random.sample(digits, 1))}"
    return result_str

print(get_random_string(12))
# write to file