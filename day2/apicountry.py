import json
import requests
import sys
import random
def country_info():
    parse = requests.get('https://restcountries.eu/rest/v2/all')
    parse = parse.json()
    country = random.randint(0,len(parse))
    flag = parse[country]['flag']
    name = parse[country]['name']
    lang = parse[country]['languages']
    Ccode = parse[country]['alpha2Code'].lower()
    language = []
    for i in range(len(lang)):
        language.append(parse[country]['languages'][i]['name'])

    langstr = "Languages Spoken:"
    wiki_name = name.strip().replace(" ", "_")
    wiki_link = "https://en.wikipedia.org/wiki/" + wiki_name


    for i in language:
        langstr=langstr + " " + i

    Name=name
    Message=langstr + ". Here is a link for our country's wikipedia page: " + wiki_link
    Icon=flag
    Icon=Ccode

    return Name, Message, Icon


if __name__=='__main__':
    Name, Message, Icon = country_info()
    print(Name)