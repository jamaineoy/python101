import unittest
import baking
class TestBaking(unittest.TestCase):

    def test_make_dough(self):
        self.assertEqual(baking.make_dough('water','flour'), True)
        self.assertEqual(baking.make_dough('wAtEr','FLour '), True)
        self.assertEqual(baking.make_dough('FlouR',' WatEr '), True)
        self.assertEqual(baking.make_dough('FlouR',' Wat3r '), False)
        self.assertEqual(baking.make_dough('wAtEr','MiLk'), False)

    def test_make_pizza(self):
        self.assertEqual(baking.make_pizza('dough'), True)
        self.assertEqual(baking.make_pizza('DoUgH'), True)
        self.assertEqual(baking.make_pizza('DoUgH '), True)
        self.assertEqual(baking.make_pizza('D0ugh'), False)
        self.assertEqual(baking.make_pizza('WeetaBix! '), False)

    def test_make_new_pizza(self):
        self.assertEqual(baking.make_dough('water','flour'), True)
        self.assertEqual(baking.make_dough('wAtEr','FLour '), True)
        self.assertEqual(baking.make_dough('wAtEr','MiLk'), False)

if __name__=='__main__':
    unittest.main()