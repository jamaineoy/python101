# Python 101
Gives the operations that are available for a variable
```python
dir(variable)
```

```python
["even" if x %2 == 0 else "odd" for x in range(20)]
```

```python
s1 = {1,3,5,7}
s2 = {2,4,6,8}
s1.union(s2)
```
```python
d = {"one":1,"two":2,"three":3}
x = d.get('five')
y = d.get('one')
type(x)
>>> <class 'NoneType'>
```
