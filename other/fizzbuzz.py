import intprompt as intp
def fizzbuzz(number):
    number = int(number)
    for i in range(0,number+1):
        if i % 3 == 0 and i % 5 != 0:
            print("Fizz")
        elif i % 3 != 0 and i % 5 == 0:
            print("Buzz")     
        elif i % 3 == 0 and i % 5 == 0:
            print("FizzBuzz")
        else:
            print(i)

class FizzBuzz:
    def __init__(self):
        self.f = False
        self.b = False

    def Fizz(self,number):
        "Checks for multiples of 3"
        if number % 3 == 0:
            self.f = True
        return self.f
    
    def Buzz(self,number):
        "Checks for multiples of 5"
        if number % 5 == 0:
            self.b = True
        return self.b

    def FizzBuzzer(self):
        "Checks for multiples of 3 and 5"
        if self.f == True and self.b == False:
            return 'Fizz'
        elif self.f == False and self.b == True:
            return 'Buzz'
        elif self.f == True and self.b == True:
            return 'FizzBuzz'
        else:
            return 'NothingSpecial'


if __name__=="__main__":
    #ßplayer = intp.prompt()
    #fizzbuzz(player)
    test = 5
    game = FizzBuzz()
    print(game.Fizz(test))
    print(game.Buzz(test))
    print(game.FizzBuzzer())