def prompt():
    "Function to display prompt, check if an integer has been entered and parses the value"
    try:
        A= input('Please enter a number: ')
        A = int(A)
    except ValueError:
        print("\n-----Invalid entry! Please enter a number!-----\n")
        prompter()
    else:
        if A < 0:
            prompt()
        else:
            return A